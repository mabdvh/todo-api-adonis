import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Project from "App/Models/Project";

export default class ProjectsController {
  public async store({ request, response }: HttpContextContract) {
    let name = request.input('name')
    // cara 1 instance model
    // let project = new Project()
    // project.name = name
    // await project.save()
    // console.log("project: ", project)
    let project = await Project.create({ name: name })
    console.log(project)
    return response.created({ message: "created" })
  }

  /**
   * @swagger
   * /api/v1/projects:
   *    get: 
   *      security:
   *        - bearerAuth: []
   *      tags: 
   *        - Projects
   *      summary: Get All Projects
   *      responses: 
   *        200: 
   *          description: success get all projects
   *          content: 
   *            application/json: 
   *              schemas: 
   *                message:
   *                  type: string
   *                  example: 
   *                    message: success fetch projects
   *                data: 
   *                  type: array
   *                  items: 
   *                    type: object
   *                    properties: 
   *                      id: integer
   *                      name: string
   *                  example:
   *                    - id: 1
   *                      name: Tutorial Adonis
   *                    - id: 2
   *                      name: Tutorial Swagger
   *        401:
   *          description: unauthorized, token needed        
   */
  public async index({ response }: HttpContextContract) {
    let projects = await Project.all()
    return response.status(200).json({ message: 'success fetch projects', data: projects })
  }

  public async show({ params, response }: HttpContextContract) {
    let project = await Project.find(params.id)
    response.status(200).json({ message: 'success', data: project })
  }

  public async update({ params, request, response }: HttpContextContract) {
    let project = await Project.findOrFail(params.id)
    project.name = request.input('name')

    project.save() // ketrigger updated_at
    return response.status(200).json({ message: 'updated' })
  }

  public async destroy({ params, response }: HttpContextContract) {
    let project = await Project.findOrFail(params.id)
    await project.delete()

    return response.status(200).json({ message: 'deleted' })
  }


}
