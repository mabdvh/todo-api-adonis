import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import Project from 'App/Models/Project';
import Status from 'App/Models/Status'

export default class StatusesController {
  public async index({ params, response }: HttpContextContract) {
    let statuses = await Status.query().where('project_id', params.project_id);
    console.log("Statuses : ", statuses)

    return response.status(200).json({ message: 'success fetch statuses', data: statuses })
  }

  public async store({ params, response, request }: HttpContextContract) {
    let projectId = params['project_id']
    let newStatus = new Status()
    newStatus.name = request.input('name')
    newStatus.project_id = projectId
    await newStatus.save()

    return response.created({ message: 'created' })
  }

  public async update({ }: HttpContextContract) {
  }

}
