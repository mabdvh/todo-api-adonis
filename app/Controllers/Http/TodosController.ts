import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

import Todo from 'App/Models/Todo'
import Tag from 'App/Models/Tag'

export default class TodosController {
  public async index({ response }: HttpContextContract) {
    const data = await Database.query().select(['id', 'task', 'due_date']).from('todos')
    // console.log("🚀 ~ file: TodosController.ts ~ line 9 ~ TodosController ~ index ~ user", user.name)
    return response.status(200).json(data)
  }

  public async show({ params, response }: HttpContextContract) {
    // let id = params.id
    // console.log("🚀 ~ file: TodosController.ts ~ line 17 ~ TodosController ~ show ~ id", id)
    // let data = await Database.from('todos').select(['id', 'task', 'due_date']).where('id', id).first();
    // console.log("🚀 ~ file: TodosController.ts ~ line 14 ~ TodosController ~ show ~ data", data)

    // Cara model sekaligus ambil relasi ke tag
    let todo = await Todo.query().where('id', params.id).preload('tags').withCount('tags').first()
    let data = JSON.stringify(todo)
    let dataAddCount = { ...JSON.parse(data), tags_count: todo?.$extras.tags_count }

    return response.status(200).json({ message: 'success', data: dataAddCount })
  }

  public async store({ request, response }: HttpContextContract) {
    let task = request.input('task')
    let dueDate = request.input('dueDate')
    let tags = request.input('tags')


    // const [lastInsertId] = await Database.table('todos').insert({ task, due_date: dueDate })

    // const data = await Database.from('todos').select(['id', 'task', 'due_date']).where('id', lastInsertId)

    // cara 2 dengan model & relationship dengan tag
    let todo = await Todo.create({ task, dueDate })
    // await todo.related('tags').createMany(tags)

    // pake sync harus ambil id nya
    // 0. dapetin data tags nya dari request input (harus jadi sebuah array berisi data tags)
    // 1. looping tags nya, buat variabel penampung id berupa array
    // 2. a. di dalam looping, dicek satu persatu tag nya jika sudah ada di database, ambil id nya
    // 2. b. jika tidak di database, maka buat dulu tag baru lalu dapetin id nya juga.
    // 3. id dari tags ditampung ke array penampung id
    // 4. array penampung id dihubungkan dengan todo melalui metode sync

    let arrayIds: number[] = []
    for (let i = 0; i < tags.length; i++) {
      const tag = tags[i];
      const tagIfExist = await Tag.firstOrCreate({ name: tag.name }, { name: tag.name })

      arrayIds.push(tagIfExist.id)

    }
    console.log("🚀 ~ file: TodosController.ts ~ line 52 ~ TodosController ~ store ~ arrayIds", arrayIds)
    // let tagsSave = await Tag.createMany(tags);


    await todo.related('tags').sync(arrayIds)
    return response.created({ message: 'created!' });
  }

  public async update({ request, response, params }: HttpContextContract) {
    let id = params.id
    let task = request.input('task')
    let due_date = request.input('dueDate')
    await Database.from('todos').where('id', id).update({ task, due_date });

    return response.status(200).json({ message: 'updated!' })
  }

  public async destroy({ params, response }: HttpContextContract) {
    await Database
      .from('todos')
      .where('id', params.id)
      .delete()

    return response.status(200).json({ message: 'deleted!' })
  }
}
