import { DateTime } from 'luxon'
import { BaseModel, column, manyToMany, ManyToMany } from '@ioc:Adonis/Lucid/Orm'
import Tag from 'App/Models/Tag'

export default class Todo extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public task: string

  @column.dateTime()
  public dueDate: DateTime

  @column()
  public statusId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @manyToMany(() => Tag, {
    localKey: 'id',
    pivotForeignKey: 'todo_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'tag_id'
  })
  public tags: ManyToMany<typeof Tag>
}
