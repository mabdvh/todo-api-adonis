import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TagTodos extends BaseSchema {
  protected tableName = 'tag_todo'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.primary(['todo_id', 'tag_id'])
      table.integer('todo_id').unsigned().references('id').inTable('todos')
      table.integer('tag_id').unsigned().references('id').inTable('tags')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
