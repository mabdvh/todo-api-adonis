import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Todos extends BaseSchema {
  protected tableName = 'todos'

  public async up() {
    this.schema.table(this.tableName, (table) => {
      table.integer('status_id')
        .unsigned()
        .references('id')
        .inTable('statuses')
    })
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {
      table.dropForeign(['status_id'])
      table.dropColumn('status_id')
    })
  }
}
